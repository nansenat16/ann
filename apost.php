<?php
include('./inc/lib.php');
include ("conf/config.inc.php");

$userauth=isset($userauth) ? $userauth : '';
if ($userauth == "xoops")
{
  if (empty($xoopspath))
    $xoopspath = "../xoops";

  include("$xoopspath/mainfile.php");
}

$session2p=isset($session2p) ? $session2p : '';
if ($session2p == "no")
{
  if (!($sn = $_GET['sn']))
    $sn = $_POST['sn'];
  
  if ($sn)
    session_id($sn); 
}

$out_see=isset($out_see) ? $out_see : '';
if ($out_see == "yes" && $userauth != "xoops")
{
  include("auth_by_ezf123.php");
  include ("conf/defconf.inc.php");
}
else
{
  include ("conf/defconf.inc.php");
  if ($use_sql_session == "yes")
    include ("inc/sessions.inc.php");

  session_start();

  if ($use_sql_session == "yes")
    include ("inc/del_session.php");
}

include ("inc/backgroup.inc.php");
include ("inc/sslhost.inc.php");

val($havehtml,$_POST['havehtml']);
val($mmtype,$_POST['mmtype']);
val($myagain,$_POST['myagain']);
val($mztype,$_POST['mztype']);
val($mysubject,$_POST['mysubject']);
val($mycomment,$_POST['mycomment']);
val($MAX_FILE_SIZE,$_POST['MAX_FILE_SIZE']);
val($myday,$_POST['myday']);
val($mypartname,$_POST['mypartname']);
val($myuserid,$_POST['myuserid']);
val($myhits,$_POST['myhits']);
val($tofirsttime,$_POST['tofirsttime']);
val($active,$_POST['active']);
val($change,$_POST['change']);
val($errorno,$_GET['errorno']);
val($isup,$_POST['myup']);


val($post_year,$_POST['post_year']);
val($post_month,$_POST['post_month']);
val($post_day,$_POST['post_day']);

// 用 JavaScript 檢查表單是否有填

$js1 = "
<script language=javascript src=check_form.js></script>
<script language=javascript>
<!--
function check()
{
    var aForm = document.add;
    var aStr = '';
";

$js2 = "
  if (aStr != '')
  {
    alert(aStr);
    return false;
  }

  return true;
}
//-->
</script>
";

$js = NULL;

val($myfilenum,$_GET['myfilenum']);
val($myfilenum,$_POST['myfilenum']);
$myfilenum=isset($myfilenum) ? abs($myfilenum) : $defulfilenum;

val($myurlnum,$_GET['myurlnum']);
val($myurlnum,$_POST['myurlnum']);
$myurlnum=isset($myurlnum) ? abs($myurlnum) : $defurlnum;

if ($myfilenum > $ulfilenum)
  $myfilenum = $ulfilenum;

if ($myurlnum > $urlnum)
  $myurlnum = $urlnum;

val($mylocal,$_GET['mylocal']);
val($mylocal,$_POST['mylocal']);


if ($use_fckeditor == "yes")
  include($fckeditorPath."/fckeditor.php");
elseif ($active != 1 && $use_ckeditor == "yes")
  echo "<script type='text/javascript' src='".$ckeditorPath."/ckeditor.js'></script>";


for ($nxnx = 0; $nxnx < $ulfilenum; $nxnx++)
{
  $fd = "fd".$nxnx;
  $$fd = isset($_POST[$fd]) ? $_POST[$fd] : '';
}

for ($nxnx = 0; $nxnx < $urlnum; $nxnx++)
{
  $myurl = "myurl".$nxnx;
  $$myurl=isset($_POST[$myurl]) ? $_POST[$myurl] : '';
}

if ($phpruntime)
  set_time_limit($phpruntime);

include ("inc/auth.inc.php");

if (!isset($userid) && $do != "edit"){
  ann_error('超級總管不可發佈公告！', 1, 1);
}


if (!function_exists('str_conv')){
  function str_conv($from, $to, $str){
    $str = trim($str);
	if (function_exists('iconv')){
      return iconv($from, $to, $str);
    }elseif (function_exists('mb_convert_encoding')){
      return mb_convert_encoding($str, $to, $from);
    }else{
      return $str;
    }
  }
}


if ($active == 1 && empty($change))
{
  if ($mylocal == 2)
    $mmlocal = "yes";
  else
    $mmlocal = "no";

  $filename = $fntmp = "";
  if ($do == "edit")
  {
// 取出其它不能修改的資料
    $link = "select filename from anntb where tid='".intval($mytid)."'";
    $sql_act = "array";
    include ("inc/sql.inc.php");
    $fntmp = $row['filename'];

    $f = "";
    $fda=array();
    $ftmp = strtok($fntmp, " ");
    $i = 0;
    while ($ftmp)
    {
      if (!empty($filename))
        $f = " ";

      $ff = "fd".$i;
      if ($$ff != 1)
        $filename .= $f.$ftmp;
      else
        $fda[] = $ftmp;
      $ftmp = strtok(" ");
      $i++;
    }
  }

  $myfilelen = strlen($filename);

// 標題和內容不可不寫
  $errorno = NULL;

  if (empty($mysubject))
    $errorno = "a";
  elseif (empty($mycomment))
   $errorno = "b";
  else
  {
    $mypath = $uploadpath."/".(!isset($userid) ? $mypartid : $partid);//超管沿用原文partid
    $mytmp = $uploadpath."/tmp";

    if (!is_dir($mytmp))
      mkdir($mytmp, 0755);

    $dir = opendir($mytmp);	// 清除 files/tmp 上傳失敗的檔案
    while($nfn = readdir($dir))
    {
      $dfn = $mytmp."/".$nfn; 
      if (!preg_match('/^\.\.?$/', $nfn) && filemtime($dfn) < time() - 86400)
        unlink($dfn);
    }
    closedir($dir);

    if (!is_dir($mypath))
      mkdir($mypath, 0755);


    $mypath .= "/".((isset($myuserid)) ? $myuserid : $userid);//如果是修改使用原userid
    if (!is_dir($mypath))
      mkdir($mypath, 0755);


    for ($i = 0; ($i < $ulfilenum && empty($errorno) && $myfilelen < 250); $i++)
    {
      $myfilename = "myfilename".$i;

      if ($file_name_use_num == "yes")
        $fname_ar[$i] = NULL;
// 取出檔名

      $ofn = "";
      $ofs = 0;
      $ofn = isset($_FILES[$myfilename]['name']) ? $_FILES[$myfilename]['name'] : NULL;
      $ofs = isset($_FILES[$myfilename]['size']) ? $_FILES[$myfilename]['size'] : NULL;

// 如果有附件時
      if ($ofn && $ofs)
      {
        $no = strtok($noupload, " ");

        while ($no)
        {	// 判斷是否為不可上傳的檔案類型
          if (preg_match('/'.$no.'/i', $ofn))
            $errorno = "c";
          $no = strtok(" ");
        }

	// 開頭不可為 .
        if (preg_match('/^\./', $ofn) || preg_match('/\.\./', $ofn))
          $errorno = "d";

	// 檔案名稱不可為特殊符號
        if (preg_match('/[ ;<\/\'\\\\]/', $ofn))
          $errorno = "e";

	// 沒有 error 才把檔案放置好
        if (empty($errorno))
        {
          $myfilelen += strlen($ofn);
          $filenameok = addslashes($ofn);

	  if ($file_name_use2_big5 == "yes")
	    $filenameok = str_conv("UTF-8", "Big5", $filenameok);
	  else if ($file_name_use2_utf8 == "yes")
	    $filenameok = str_conv("Big5", "UTF-8", $filenameok);

          if ($file_name_use_num == "yes")
          {
            $f_ar = explode(".", $filenameok);
            $f_form = $f_ar[count($f_ar)-1];

            $filenameok = time() . $i .".". $f_form;
            $fname_ar[$i] = $filenameok;
          }

          $myfile = $mypath."/".$filenameok;
          $myfile1 = $mytmp."/".$filenameok;
          if (file_exists($myfile))
          {
            $errorno = 'f';
            unlink($_FILES[$myfilename]['tmp_name']);
          }
          elseif (!(move_uploaded_file($_FILES[$myfilename]['tmp_name'], $myfile1) && chmod($myfile1, 0644)))
            $errorno = 'g';
        }
      } // if
    } //for

    if ($myfilelen > 250)
      $overi = $i - 1;
    else
      $overi = $i;
  }

  if ($mztype == 2)
    $mztype = 0.1;
  else
    $mztype = 0;
  $mytype = $mmtype + $mztype;
  if (!empty($errorno))	// 有錯時，重新發佈
  {
    for ($i = 0; $i < $urlnum; $i++)
    {
      $mm = "myurl".$i;
      $_SESSION[$mm] = $$mm;
    }

    $_SESSION['mysubject'] = $mysubject;
    $_SESSION['mycomment'] = $mycomment;
    $_SESSION['havehtml'] = $havehtml;
    $_SESSION['myday'] = $myday;
    $_SESSION['mytype'] = $mytype;

    $_SESSION["post_year"] = $post_year;
    $_SESSION["post_month"] = $post_month;
    $_SESSION["post_day"] = $post_day;

    header("Location: {$_SERVER['PHP_SELF']}?mypartid=$mypartid&mytid=$mytid&errorno=$errorno&do=$do&myfilenum=$myfilenum&myurlnum=$myurlnum&mylocal=$mylocal");
    exit;
  }


  $subject = str_replace("&amp;#", "&#", htmlspecialchars(addslashes(trim($mysubject))));

  if ($htmlcode != "yes")
    $havehtml = 0;	// 確保一下:p


  if (empty($max_text_size))
      $max_size = 64 * 1024 - 8;
  else
    $max_size = $max_text_size * 1024 - 8;

  if ($havehtml == 1)
    $comment = substr(addslashes($mycomment), 0, $max_size);
  else
    $comment = substr(str_replace("&amp;#", "&#", htmlspecialchars(addslashes($mycomment))), 0, $max_size);

  $u = $url ='';
  $myurllen = 0;
  for ($i = 0; $i < $urlnum; $i++)
  {
    if (!empty($url))
      $u = " ";
    $mm = "myurl".$i;

    $tmpurl = preg_replace('/ /', '', $$mm);
    $myurllen += strlen($tmpurl);
    if ($tmpurl && $myurllen < 250)
      $url .= $u.$tmpurl;
  }

  
  $url = preg_replace('/<[^>]*>/', '', addslashes($url));

  if ($do == "edit")
  {
    if (!(!isset($userid) || $rootuid == $userid)) // 不是超管和群組長
    {
      $myuserid = $userid; // 用真正的 userid
      $mypartid = $partid; // 用真正的 partid
    }
    $partname = htmlspecialchars(addslashes($mypartname));
  }
  else
  {
    $myuserid = $userid; // 用真正的 userid
// 取群組名稱
    $link = "select partname from parttb where partid='".intval($mypartid)."'";
    $sql_act = "array";
    include ("inc/sql.inc.php");

    if ($usegmshow == "yes")
    {
      $partname1 = $row['partname'];

// 取 realname
      $link = "select realname from usertb where userid='".intval($myuserid)."'";
      $sql_act = "array";
      include ("inc/sql.inc.php");

      $realname = $row['realname'];

      $partname = $realname . "(" . $partname1 . ")";
    }
    else
      $partname = $row['partname'];

  }

  if ($use_date == "yes")
    $myday = intval((mktime(0, 0, 0, $post_month, $post_day, $post_year) - time()) / 86400 + 1);

// 算天數
  include ("inc/day.inc.php");

  $overtime = date("Y-m-d", (time() + $day * 86400));
  $nowtime = date("Y-m-d H:i:s");


  $f = "";
  for ($i = 0; ($i < $ulfilenum && $i < $overi); $i++)
  {
    if (!empty($filename))
      $f = " ";

    $myfilename = "myfilename".$i;
    $ofn = "";
    $ofs = 0;
    $ofn = isset($_FILES[$myfilename]['name']) ? $_FILES[$myfilename]['name'] : NULL;
    $ofs = isset($_FILES[$myfilename]['size']) ? $_FILES[$myfilename]['size'] : NULL;

    if ($ofn && $ofs)
    {
      $fn = addslashes($ofn);
      if ($file_name_use_num == "yes")
        $fn = $fname_ar[$i];

      $filename .= $f.$fn;
    }
  }
  
  if ($do == "edit")
  {
    if ($resethit == "yes")
      $myhits = 0;

    if (!isset($userid))
      $mytmpss = "up='$isup',";
    else
      $mytmpss = "firsttime='$tofirsttime',";

    $link = "update titletb set subject='$subject ', partname='$partname ', posttime='$nowtime', overtime='$overtime',". $mytmpss ." hits='$myhits', type='$mytype', local='$mmlocal' where tid='$mytid'";
    $sql_act = "do";
    include ("inc/sql.inc.php");

// 刪除檔案
    $n = count($fda);
    for ($i = 0; $i < $n; $i++)
    {
// bug 1_41
       if ($file_name_use2_big5 == "yes"){
         $ff = str_conv("UTF-8", "Big5", $fda[$i]);
       }else if ($file_name_use2_utf8 == "yes"){
         $ff = str_conv("Big5", "UTF-8", $fda[$i]);
	   }else{
	   	 $ff = $fda[$i];
	   }
	   
       $mydelfile = $mypath."/".$ff; 
       unlink($mydelfile);
    }

    $link = "update anntb set userid='$myuserid', ip='$myip', filename='$filename', url='$url', comment='$comment' where tid='$mytid'";
    $sql_act = "do";
    include ("inc/sql.inc.php");

    $logmsg = "修改 ".$mysubject;

  }
  else
  {
    $myhits = 0;

// 加入至 title 中
    $link = "insert into titletb (partid, partname, subject, posttime, overtime, hits, type, local, firsttime) values ('$mypartid', '$partname ', '$subject ', '$nowtime', '$overtime', '$myhits', '$mytype', '$mmlocal', '0000-00-00 00:00:00')";
    $sql_act = "do";
    include ("inc/sql.inc.php");
 
// 取 title 中的 tid
    $link = "select last_insert_id()";
    $sql_act = "array";
    include ("inc/sql.inc.php");
    $tid = $row[0];

// 加入至公告內容中
    $link = "insert into anntb (tid, userid, ip, filename, url, comment) values ('$tid', '$myuserid', '$myip', '$filename', '$url', '$comment ')";
    $sql_act = "do";
    include ("inc/sql.inc.php");

    $logmsg = "發佈 ".$subject;
  }

// 做記錄
  include ("inc/log.inc.php");

// RSS
  include ("inc/rss.inc.php");

// 將檔案真正放置

  for ($i = 0; ($i < $ulfilenum && $i < $overi); $i++)
  {
    $myfilename = "myfilename".$i;
    $ofn = "";
    $ofs = 0;
    $ofn = isset($_FILES[$myfilename]['name']) ? $_FILES[$myfilename]['name'] : NULL;
    $ofs = isset($_FILES[$myfilename]['size']) ? $_FILES[$myfilename]['size'] : NULL;

    if ($ofn && $ofs)
    {
      $filenameok = addslashes($ofn);

      if ($file_name_use2_big5 == "yes")
        $filenameok = str_conv("UTF-8", "Big5", $filenameok);
      else if ($file_name_use2_utf8 == "yes")
        $filenameok = str_conv("Big5", "UTF-8", $filenameok);

      if ($file_name_use_num == "yes")
      {
        $myfile = $mypath."/".$fname_ar[$i];
        $myfile1 = $mytmp."/".$fname_ar[$i];
      }
      else
      {
        $myfile = $mypath."/".$filenameok;
        $myfile1 = $mytmp."/".$filenameok;
      }

      rename($myfile1, $myfile);
    }
  }

// 回公告系統

  if ($myagain == 2)
  {
     $tmp_ss = NULL;
     if ($session2p == "no")
       $tmp_ss = "&sn=".session_id();
     header("Location: $myhostssl/apost.php?mypartid=$mypartid$tmp_ss");
     exit;
  }
  include ("exit.php");

  exit;
} 
// 程式開始

echo $lang_title;

if ($bg_rand != "yes")
{
  $mybg = bg($addbg);
}
else
{
  include ("inc/bgrand.inc.php");

  $mybg = bg($outbg);
}

?>
<script language="JavaScript" type="text/JavaScript">
<!--
function setfocus()
{
document.forms[0].mysubject.focus();
}

//-->
</script>
<?php



echo '<body onLoad="setfocus();" '.$mybg.'><center>';

if ($do == "edit")
{
// 檢查身份
  include ("inc/owner.inc.php");
}

if ($add_check == "yes")
  echo '<form name="add" enctype="multipart/form-data"  method="POST" action="'.$_SERVER['PHP_SELF'].'" onsubmit="return check()">';
else
  echo '<form name="add" enctype="multipart/form-data"  method="POST" action="'.$_SERVER['PHP_SELF'].'">';

if ($do == "edit")
  echo '<input type="hidden" name="mytid" value="'.intval($mytid).'">';

if ($session2p == "no")
  echo '<input type="hidden" name="sn" value="'.htmlspecialchars(session_id()).'">';

?>
  <input type="hidden" name="active" value="1">
  <input type="hidden" name="do" value="<?php echo htmlspecialchars($do); ?>">
  <input type="hidden" name="mypartid" value="<?php echo intval($mypartid); ?>">

<?php
include ("inc/myhost.inc.php");

echo '<a href="'.$myhost.'/exit.php">登出並返回公告系統</a>';

include ("inc/ident.inc.php");
echo '　　<input type="submit" value="送出公告"><br>';

if (!empty($errorno))
{
  $a = "標題尚未填寫";
  $b = "內容尚未填寫";
  $c = "附件副檔名錯誤";
  $d = "附件名稱開頭不可為點(.)";
  $e = "附件名稱不可有空白等特殊符號";
  $f = "附件檔名不可與之前上傳的相同";
  $g = "附件上傳失敗，請洽系統管理員";
  echo "<font color=red>錯誤訊息：公告".$$errorno."，請重新發佈！</font>";
  echo '<script type="text/javascript">';
  echo "{alert('".$$errorno."，請重新發佈！');}";
  echo '</script>';
} 

$myday = $annday;

if ($do == "edit")
{
  $link = "select * from titletb where tid='".intval($mytid)."'";
  $sql_act = "array";
  include ("inc/sql.inc.php");
  $mypartname = stripslashes($row['partname']);
  $mysubject = stripslashes($row['subject']);
  $mytype = $row['type'];
  $myhits = $row['hits'];
  $tofirsttime = $firsttime = $row['firsttime']; 
  $ot = $row['overtime']; 
  $mmlocal = $row['local']; 

  $ot = explode(" ",$ot);
  $myot1 = explode("-",$ot[0]);
  $myot2 = explode(":",$ot[1]);

  $myday = intval((mktime($myot2[0], $myot2[1], $myot2[2], $myot1[1], $myot1[2], $myot1[0]) - time()) / 86400 + 1);

  $mydate = explode(" ",$firsttime);
  $mydate = explode("-",$mydate[0]);

  if ($mydate[0] == "0000")
    $tofirsttime = $row['posttime']; 

  $link = "select * from anntb where tid='".intval($mytid)."'";
  $sql_act = "array";
  include ("inc/sql.inc.php");
  $filename = stripslashes($row['filename']);
  $myurl = stripslashes($row['url']);
  $myuserid = $row['userid'];
  $mycomment = stripslashes($row['comment']);

  if (preg_match('/</', $mycomment))
    $reedith = "yes";
  else
    $reedith = "no";

  echo "<input type=\"hidden\" name=\"mypartname\" value=\"$mypartname\">";
  echo "<input type=\"hidden\" name=\"myuserid\" value=\"$myuserid\">";
  echo "<input type=\"hidden\" name=\"myhits\" value=\"$myhits\">";
  echo "<input type=\"hidden\" name=\"tofirsttime\" value=\"$tofirsttime\">";
}
else
{


  val($mytype,$_SESSION['mytype']);
  val($mysubject,$_SESSION['mysubject']);
  val($mycomment,$_SESSION['mycomment']);
  val($myday,$_SESSION['myday']);
  val($post_year,$_SESSION['post_year']);
  val($post_month,$_SESSION['post_month']);
  val($post_day,$_SESSION['post_day']);

  if (empty($myday))
    $myday = $annday;
}

$mmtype = intval($mytype);

$mybg = bg($addtable);
echo '<table border="1" cellspacing="0" cellpadding="0" '.$mybg.'>';

$mz1 = $mz2 = "";
if (!empty($mytype))
{
  if (($mytype * 10) % 10 == 1)
    $mz2 = "selected";
  else
    $mz1 = "selected";
}
else
{
  if ($toez == "yes")
    $mz2 = "selected";
  else
    $mz1 = "selected";
}

$mytmptt = "";
if (!isset($userid))
  $mytmptt = "置頂：<select name='myup' size='1'>
    <option value='0' selected>否</option>
    <option value='1' >是</option>
  </select>";

?>
<tr><td width="15%" align="center">公告等級
  <select name="mmtype" size="1">
    <option value="1" <?php echo ($mmtype==1) ? 'selected' :''; ?>>普通</option>
    <option value="2" <?php echo ($mmtype==2) ? 'selected' :''; ?>>重要</option>
    <option value="3" <?php echo ($mmtype==3) ? 'selected' :''; ?>>急件</option>
  </select></td>

</td><td width="85%">
連續發公告：
  <select name="myagain" size="1">
    <option value="1" selected>否</option>
    <option value="2" >是</option>
  </select>
加入簡易主頁中：
  <select name="mztype" size="1">
    <option value="1" <?php echo $mz1; ?>>否</option>
    <option value="2" <?php echo $mz2; ?>>是</option>
  </select>
內部公告：
<?php
$mmlocal=isset($mmlocal) ? $mmlocal : '';
$tmp_local=($mylocal == '2' || $mmlocal == 'yes');
?>
  <select name="mylocal" size="1">
    <option value="1" <?php echo (!$tmp_local) ? 'selected' : ''; ?>>否</option>
    <option value="2" <?php echo ($tmp_local) ? 'selected' : ''; ?>>是</option>
  </select>
<br>
<?php echo $mytmptt;?> 標題：
   <input type="text" name="mysubject" size="40" maxlength="<?php echo $max_subject; ?>" value="<?php echo $mysubject ?>"></td></tr>
<tr><td colspan="2">

<?php
if ($use_fckeditor == "yes")
{
  echo '<font color="red">★標題和內容一定要寫！</font>';

  if ($htmlcode == "no")
  echo " (HTML 語法關閉，請洽管理員！)<br>";

  $oFCKeditor = new FCKeditor('mycomment') ;
  $oFCKeditor->BasePath = $fckeditorPath.'/';
  $oFCKeditor->Value = $mycomment;

  $oFCKeditor->Width  = $fckeditorWidth;
  $oFCKeditor->Height = $fckeditorHeight;

  $oFCKeditor->Create() ;

}
elseif ($use_ckeditor == "yes")
{
  echo '<font color="red">★標題和內容一定要寫！</font>';
  echo '<textarea name="mycomment" rows="15" cols="80" wrap="off">'.$mycomment.'</textarea>';

  echo "<script type='text/javascript'>
  	CKEDITOR.replace('mycomment');
	</script>";
}
else
{
?>
<font color="red">
★標題和內容一定要寫！
</font>
<?php
if ($htmlcode == "yes")
  echo " (若要使用HTML 語法，相關選項請選是，換行請用 &lt;br&gt;)<br>";
else
  echo " (HTML 語法關閉)<br>";

?>


   <textarea name="mycomment" rows="15" cols="80" wrap="off" onkeydown="if(event.keyCode==13&&event.ctrlKey) add.submit()"><?php echo $mycomment ?></textarea>


<?php
   }

  $js .= "aStr += chk_null(aForm.mysubject.value, '標題不可空白！');";
  $js .= "aStr += chk_null(aForm.mycomment.value, '內容不可空白！');";

?>

</td></tr>
  <tr><td colspan="2">

<font color="red">相關連結如要註解請用 "!" 隔開</font>
  </td></tr>
<tr><td align=center>相關檔案</td><td>
<?php
$tmpmax = $filemaxsize * 1024 * 1024;
echo '<input type="hidden" name="MAX_FILE_SIZE" value="'.intval($tmpmax).'" />';
$ni = 0;
if ($do == "edit")
{
  $ff = strtok($filename, " ");
  while ($ff)
  {
    echo "已有附件".($ni+1)."：".$ff."　刪除<input type=\"radio\" name=\"fd".$ni."\" value=\"0\" checked>否<input type=\"radio\" name=\"fd".$ni."\" value=\"1\">是<br>";
    $ff = strtok(" ");
    $ni++; 
  }
}

//$myfilelen = strlen($filename);
//if ($myfilelen < 240)
  for ($i = 0; $i < $myfilenum - $ni; $i++)
  echo "附件".($i+1)."：<input type=\"file\" name=\"myfilename".$i."\" size=\"40\" maxlength=\"250\"><br>";

?>

</td></tr><tr><td align="center">相關連結
</td><td>
<?php


$ni = 0;
if ($do == "edit")
{
  $uu = strtok($myurl, " ");
  while ($uu)
  {
    $tmpu = ${"myurl".$ni} = $uu;
    echo "網址".($ni+1)."：<input type=\"text\" size=\"52\" name=\"myurl".$ni."\" maxlength=\"250\" value=\"".$tmpu."\"><br>";
    $uu = strtok(" ");
    $ni++; 
  }
}

$myurllen = strlen($myurl);
if ($myurllen < 240)
  for ($i = 0; $i < $myurlnum - $ni; $i++)
  {
    $ii = $i + $ni;
    $mm = "myurl".$ii;
    echo "網址".($ii+1)."：<input type=\"text\" size=\"52\" name=\"myurl".$ii."\" maxlength=\"250\" value=\"".$$mm."\"><br>";
  }

?>
</td></tr>


<tr> <td align="center">公告時效
</td>
<td>
<?php
  if ($use_date == "yes")
  {
    $now_year = date("Y");
    $now_month = intval(date("m"));
    $now_day = intval(date("d"));

    $now_month_day = intval(date("t"));

    $over_year = isset($myot1[0]) ? intval($myot1[0]) : 0;
    $over_month = isset($myot1[1]) ? intval($myot1[1]) : 0;
    $over_day = isset($myot1[2]) ? intval($myot1[2]) : 0;

    if (empty($over_year))
      $over_year = $now_year;

    if (empty($over_month))
      $over_month = $now_month;

    if (empty($over_day))
      $over_day = $now_day + $annday;

    while ($over_day > $now_month_day)
    {
      $over_day -= $now_month_day;
      $over_month++;

      if ($over_month > 12)
      {
        $over_month -= 12;
        $over_year++;
      }
    }

    echo "<select name=\"post_year\" size=\"1\">";
    for ($n = 0; $n <= $maxyear; $n++)
    {
      $y = $now_year + $n;
      $p_s = "";
      if ($y == $over_year)
        $p_s = "selected";
      echo "<option value=". $y ." ". $p_s .">".$y."</option>";
    } 
    echo "</select>年";

    echo "<select name=\"post_month\" size=\"1\">";
    for ($n = 1; $n < 13; $n++)
    {
      $p_s = "";
      if ($n == $over_month)
        $p_s = "selected";
      echo "<option value=". $n ." ". $p_s .">".$n."</option>";
    } 
    echo "</select>月";

    echo '<select name="post_day" size="1">';
    for ($n = 1; $n < 32; $n++)
    {
      $p_s = "";
      if ($n == $over_day)
        $p_s = "selected";
      echo "<option value=". $n ." ". $p_s .">".$n."</option>";
    } 
    echo "</select>日 ";
  }
  else
    echo '<input type="text" size="4" name="myday" value='. $myday .' maxlength="4">天　';


echo "使用 HTML 語言：";
if ($htmlcode == "yes" || $use_fckeditor == "yes" || $use_ckeditor == "yes")

{
  $HY = $HN = NULL;
  $tmp_havehtml=isset($_SESSION['havehtml']) ? intval($_SESSION['havehtml']) : 0;
  $tmp_reedith=isset($reedith) ? $reedith : '';
  $tmp_use_html=$tmp_no_html='';
  if(($tmp_havehtml==1||$tmp_reedith=='yes')||($use_fckeditor=='yes'||$use_ckeditor=='yes')){
  	$tmp_use_html='selected';
  }else{
  	$tmp_no_html='selected';
  }

  echo '<select name="havehtml" size="1">';
  echo '<option value="0" '.$tmp_no_html.'>否</option>';
  echo '<option value="1" '.$tmp_use_html.'>是</option></select>';
}
else
  echo "否";
?>
　　<input type="submit" value="送出公告">
</td>
</tr>
</table></form>


<?php  echo $js1.$js.$js2; ?>
請留意以下幾點的注意事項：<br>
1. 附件大小共不可超過 <?php echo $filemaxsize ?>MB。<br>
2. 不可上傳中毒的附件，請用防毒軟體掃乾淨。<br>
3. 附件檔名不可有空白或 < 或 ' 等特殊符號。
</center>
</body>
<?php include ("inc/exitdoc.inc.php"); ?>
