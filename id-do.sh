#!/bin/sh

FID="u"		#帳號開頭為
PID="1"		#group id
FNUM="1"	#流水號開頭
LNUM="40"	#流水號結尾 最大 99


PWD="test"	#密碼

awk 'BEGIN{
  for (i = '$FNUM'; i <= '$LNUM'; i++)
  if (i < 10)
    print "insert into usertb (partid, uid, username, realname, userpass, email, userident) values (\"'$PID'\", \"test\", \"'$FID'0"i"\", \"'$FID'0"i"\", \"'$PWD'\", \"\", \" \");"
  else
    print "insert into usertb (partid, uid, username, realname, userpass, email, userident) values (\"'$PID'\", \"test\", \"'$FID'"i"\", \"'$FID'0"i"\", \"'$PWD'\", \"\", \" \");"
}'
