<?php
$hide_admin_id = "no";
include("conf/config.inc.php");

$tmp_host = $myhost;

if ($out_see == "yes" && $userauth != "xoops")
{
  include("auth_by_ezf123.php");
  include ("conf/defconf.inc.php");
}
else
{
  include ("conf/defconf.inc.php");
  if ($use_sql_session == "yes")
    include ("inc/sessions.inc.php");

  session_start();

  if ($use_sql_session == "yes")
    include ("inc/del_session.php");
}


if (!($userauth == "xoops" || $hide_admin_id != "no"))
  session_destroy();

$myhost = $tmp_host;

if (strstr($_SERVER['PHP_SELF'], "plz_change_me.php"))
{
  echo "請先更改此檔名才可進行認證！";
  exit;
}

if ($hide_admin_id != "no")
{
  $id_data = $hide_admin_id.strlen($hide_admin_id);
  $_SESSION[$hide_admin_id] = $id_data;
}

header("Location: $myhost/index.php");
exit;

?>
