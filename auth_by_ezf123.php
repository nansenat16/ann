<?php

$ezf123path = "../ezf123";		/* ezF123 的路徑 */
$pgname = "校內公告登入";		/* 套用 ezF123 認證系統的程式名稱 */

/*
□ 使用方法：
1. 將此檔複製至需用 ezF123 認證系統的目錄中

  cp auth_by_ezf123.php <需用 ezF123 認證系統的目錄>

2. 首頁一定要叫 index.php
  如果您的首頁不是 index.php 的話(如果叫 index.php3)，請用 ln 做 soft link, 下

  ln -s index.php3 index.php

3. 請在 index.php 的最開頭加

  include("auth_by_ezf123.php");
  session_destroy();

4. 在需要認證的 php 檔 include auth_by_ezf123.php 進來(之前需加 $auth = 1)，如

  $auth = 1;
  include("auth_by_ezf123.php");

5. 所有的設定均抓取 ezF123 的設定檔
  如果非得不一樣時，請在下頭 include("$ezf123path/inc/defconf.inc.php"); 此行
  下方加入您需要改變的參數設定。

6. 如果有參數(如 $do)要傳進去的話，請在此檔最下頭的 if ($auth == 1) 中加入
   $_SESSION['do'] = $do;

7. 可用的參數有(當初在 ezF123 中所設定的使用者資料)

  $my_user	帳號
  $my_pwd		密碼
  $superuser	是否為 ezF123 的超者(1 是, 0 否)
  $my_gid		群組
  $my_uname	中文名字
  $my_admin	是否為管理者(1 是, 0 否)

*/

/* 以下請不要更改 */

include("$ezf123path/inc/defconf.inc.php");
if ($usesqlsession == "yes")
{
  include("$ezf123path/inc/sessions.inc.php");
}

session_start();

if ($auth == 1)
{
// $_SESSION['do'] = $do;
  include("$ezf123path/inc/auth.inc.php");
}

?>
