function chk_null(column, name)
{
  var stat = 0;

  if (column.length == 0)
    return name + "\n";

  for (var i = 0; i < column.length; i++)
    if (column.charAt(i) != ' ')
    {
      stat = 1;
      break;
    }

  if (stat == 1)
    return "";
  else
    return name + "\n";
}

