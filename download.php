<?php
include ("conf/defconf.inc.php");

$dpid = $_GET['dpid'];
$duid = $_GET['duid'];
$mytid = $_GET['mytid'];
$dfn = $_GET['dfn'];

function str_conv($from, $to, $str)
{
  $str = trim($str);

  if (function_exists('mb_convert_encoding'))
    return mb_convert_encoding($str, $to, $from);
  elseif (function_exists('iconv'))
    return iconv($from, $to, $str);
  else
    return $str;
}


$dfn = preg_replace('/\//', '', preg_replace('/ /', '', $dfn));

if ($file_name_url_big5 == "no")
  $dfn = str_conv("UTF-8", "Big5", $dfn);

if (!preg_match('/^[0-9]+$/', $dpid) || !preg_match('/^[0-9]+$/', $duid) || preg_match('/\.\./', $dfn))
{
  die('你亂來！');
}

if ($safe_dl == "yes")
{
  $link = "select * from anntb where tid='".intval($mytid)."'";
  $sql_act = "array";
  include ("inc/sql.inc.php");
  $filename = stripslashes($row['filename']);

  if (!empty($filename))
  {
    $ff = strtok($filename, " ");

    $i = 0;
    while ($ff)
    {
      if ($dfn == $i)
      {
        if ($file_name_use2_big5 == "yes")
          $ff = str_conv("UTF-8", "Big5", $ff);
        else if ($file_name_use2_utf8 == "yes")
          $ff = str_conv("Big5", "UTF-8", $ff);

        $dlf = $uploadpath."/".$dpid."/".$duid."/".$ff;
        break;
      }

      $ff = strtok(" ");
      $i++;
    }
    $dfn = $ff;
  }
  else
    echo "此公告沒有附件！";
}
else
  $dlf = $uploadpath."/".$dpid."/".$duid."/".$dfn;

if (file_exists($dlf))
{
  if ($file_name_dl2_utf8 == "yes")
    $dfn = str_conv("Big5", "UTF-8", $dfn);
  else if ($file_name_dl2_big5 == "yes")
    $dfn = str_conv("UTF-8", "Big5", $dfn);

  $file_size = filesize($dlf);
  Header("Content-Disposition: attachment; filename=".$dfn);
  Header("Content-Type: APPLICATION/octet-stream");
  Header('Content-Length: ' . $file_size);
  Header('Content-Transfer-Encoding: binary');

  readfile($dlf);

}
else
  echo "沒有 ".$dfn." 這個檔案！";
?>
