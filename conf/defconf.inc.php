<?php

// 這是預設值專用檔，請不要對此檔做任何修改！
// 如 config.inc.php 沒有設定，才會使用本檔的預設值。
// 不想用預設值，請將該參數 copy 至 config.inc.php 來設定。


// 一次砍過期公告多少封後，資料庫將會重整 (For mysql 專用)
$numberdel = 50;

// 網站的資料
//$mypicname = "title.gif";		/* 主頁 title 的圖案 */
$showip = "no";		/* yes/no/其他訊息 是否秀出來源 */

// 一般性設定
$usegmshow = "no";		/* 使用群組分類時，在首頁列出組員姓名 */
$ezshowday = 30;		/* 簡易公告有效天數 */
$htmlcode = "yes";		/* yes/no 使用 HTML 語法 */
$space = "yes";			/* yes/no 將空白換成 &nbsp; */
$limit = 20;			/* 每頁可顯示幾篇消息 */
$annday = 14;			/* 預設公告時間(單位：天) */
$searchday = 666;		/* 預設搜尋時間(單位：天) */
$maxday = 999;			/* 預設公告最長時間(單位：天) */
$maxyear = 3;			/* 預設公告可選擇最長時間(單位：年) */
$reloadtime = 10;		/* 主畫面幾分鐘重讀一次 */
$resethit = "no";		/* yes/no 編修公告後人氣是否歸零 */
$ulfilenum = 9;			/* 附件個數(檔名愈長，支援個數愈少) */
$urlnum = 9;			/* 相關網址個數(綱址愈長，支援個數愈少) */
$defulfilenum = $ulfilenum;	/* 預設附件個數 */
$defurlnum = $urlnum;		/* 附件網址個數 */
$indexhavepage = "no";		/* yes/no 主頁是否分頁 */
$groupisc = "no";		/* yes/no 主頁的群組是否置中 */
$hitsisc = "no";		/* yes/no 主頁的人氣是否置中 */
$phpruntime = 0;		/* 發公告時給予運作的時間(秒) */
$ez_typefirst = "no";		/* yes/no 簡易首頁是否將重要公告先擺在前頭？ */
$typefirst = "no";		/* yes/no 是否將重要公告先擺在前頭？ */
$toez = "yes";			/* yes/no 公告預設是否放入簡易主頁中 */
$search_txt = "no";		/* yes/no 是否搜尋內容 */
$tid_show = "no";		/* yes/no 是否列出公告編號 */
$exptime_show = "no";		/* yes/no 是否列出公告過期天數 */
$tsize = "9";			/* 所有字的大小 */
$ezindex_sort = "no";		/* 簡易首頁是否依 急件 重要 普通 排序 */
$index_sort = "no";		/* 首頁是否依 急件 重要 普通 排序 */
$new_day = "2";			/* 最近幾天出現 new 字樣 */
$max_subject = "80";		/* 最大標題字數，最大不要超過 120 */
$use_date = "yes";		/* 公告天數是否用日期 */
$use_update_type = "no";	/* 是否使用更正判別 */
$local_type = "[內部]";		/* 內部公告代號或圖案 */
$rss_days = "7";		/* 多久天前的公告才列入 RSS 中 */
$rss_img = "rss.gif";		/* rss 的圖像檔名 */
$rss_local_type = "[內部]";	/* rss 用內部公告字樣*/
$rss_type_use = "yes";		/* yes/no 是否使用類別 */
$add_check = "yes";			/* 發表公告是否先行用 java 檢查是否有填寫？ */
$logout = "no";		/* 使用 logout 模式，保留登入資訊 */

/* session 可以誇頁嗎？(session.use_trans_sid) */
// 在 XP 用 IIS/PHP 要設 no
$session2p = "yes";

$use_sql_session = "no";	// 使用 sql session 機制
$my_session_life_sec = 86400;   // 多少秒後過期, 0 表用 php.in 設定值(1440)

$nowtimestamp = time();       /* 目前時間戳印 */


// 如果要使用所見即所得編輯器，請至 http://ckeditor.com/ 下載
// 解開後資料夾 fckeditor 放至 ann 目錄下，並將 $use_fckeditor 設成 yes 即可。
// FCKeditor 2.6.5 如要上傳圖片，
// a. 修改 fckeditor/fckconfig.js (2.6.5 版預設已為 php)
//    var _FileBrowserLanguage = 'php';
//    var _QuickUploadLanguage = 'php';
// b. 在 fckeditor 中，建立 userfiles 目錄(1777 或 owner 為 httpd 可讀寫權限)
// c. 修改 fckeditor/editor/filemanager/connectors/php/config.php
//    $Config['Enabled'] = true;
//    $Config['UserFilesPath'] = '/ann/fckeditor/userfiles/';
//    如 FCKeditor 為舊版的(config.php 在不同處)，要改兩個檔
//    fckeditor/editor/filemanager/upload/php/config.php
//    fckeditor/editor/filemanager/browser/default/connectors/php/config.php
$use_fckeditor = "no";		/* 是否使用 fckeditor 所見即所得編輯器 */
$fckeditorWidth = "100%";	/* 寬度，一般都設 100% */
$fckeditorHeight = "500";	/* 高度 */
$fckeditorPath = "fckeditor";	/* fckeditor 目錄在何處(相對目錄表示) */

// 解開後資料夾 ckeditor 放至 ann 目錄下，並將 $use_ckeditor 設成 yes 即可。
$use_ckeditor = "no";		/* 是否使用 ckeditor 所見即所得編輯器 */
$ckeditorPath = "ckeditor";	/* ckeditor 目錄在何處(相對目錄表示) */

/* 選擇校內圖案 */
//$local_type = "<img src=".$myhost."/images/local.gif border=0>";

// 網頁utf8版，而檔案系為big5時， file_name_use2_big5 請設 yes
// 反之，設另一個(二選一來設定)。如都是 big5版，則不用改。
$file_name_use2_big5 = "no";	/* 讀取下載的檔案名稱是否轉為 big5 */
$file_name_use2_utf8 = "no";	/* 讀取下載的檔案名稱是否轉為 big5 */

// 下載檔名不正確(中文)請二選一設 yes
$file_name_dl2_big5 = "no";	/* 下載檔案名稱轉為 big5 */
$file_name_dl2_utf8 = "no";	/* 下載檔案名稱轉為 utf8 */


$safe_dl = "yes";		/* 使用資料庫檔名下載 */
$file_name_url_big5 = "yes";	/* 網址列下載檔案名稱是否為 big5 */

$file_name_use_num = "no";	/* 檔案名稱是否以亂數呈現 */
$url_download = "no";				/* 直接下載 */
$star_download = "no";				/* 直接下載在★顯示 */


$open_use_java = "no";		/* 開公告是否用 java sript 看 */
$open_java_width = "600";		/* 用 java 開的寬 */
$open_java_height = "600";		/* 用 java 開的高 */


// 後端認證識別碼，no 則不使用，要使用，請輸入其它英文字串，例：
// $hide_admin_id = "Ljxjkeb";
$hide_admin_id = "no";
	// 使用後，要先執行後端認證系統(plz_change_me.php)才行進行理管動作！
	// 如果要使用，請更改上述檔名，不然就失去意義啦~~~ ^o^

$usereal = "no"; 	        /* yes/no 使用 Unix 或 Xoops 帳號密碼 */
/* 如果要用系統帳號，以下四個認證方式，請挑一種支援的來用
   imap         使用 Unix 系統帳號 imap fsockopen() 認證
   pop          使用 Unix 系統帳號 pop3 fsockopen() 認證
   pimap        使用 Unix 系統帳號 php with imap 認證
   ppop         使用 Unix 系統帳號 php with pop3 認證
   xoops	使用 Xoops 認證(2.013平台測的)
*/
$xoopspath = "../xoops";	// Xoops 的路徑(mainfile.php 放在哪)
$userauth = "pop";

// 多重 pop server 認證
// 陣列第一欄為伺服器代碼，可隨意取，如 host_a
// 帳號如為 nextime@host_a 或 nextime 則會到 localhost 認證
// 帳號如為 nextime@b 則會到 192.168.3.1 認證
$authhost = array (
  "host_a" => "localhost:pop3:110",
  "b" => "192.168.3.1:pop3:110"
);  // "EMail server nickname" => " Auth IP:pop3:110",  (pop3:110, imap:143)

// 單一 pop server 認證
$authhost = "localhost";        /* 帳號在哪台伺服器上 */


// local 變數是特定公告給特定的網域或 IP 才可看用，請用空白隔開，如
// $local = "192.168.0.1 192.168.1. 192.168.2.1-125";
$local = "your.lan.ip";

/* 虛擬IP伺服器的對外IP(兩部以上，請用空白分隔) */
$natip = "your.wan.ip";

// 配合 ezF123/Xoops 認證觀看內部公告
$out_see = "no";		/* no/yes 讓成員在外面可看內部公告 */

// 請配合 php.ini 中的 upload_max_filesize 設定
//upload_max_filesize = 2097152       ; 2 Meg default limit on file uploads
$filemaxsize = 1.5;		/* 上傳附件最大容量 (MB) */

// 亂數取背景
// 如 bg_rand_name 設 tree_bg.jpg，bg_rand_num 設 6 時，
// 表在 images 中應有 0tree_bg.jpg, 1tree_bg.jpg,... 至 6tree_bg.jpg 個圖案，
// 則系統會按亂數取該 7 張背景來用。
$bg_rand = "yes";		/* yes/no 使用背景亂數功能 */
$bg_rand_name = "tree_bg.jpg";	/* 背景亂數檔名 */
$bg_rand_num = 6;		/* 背景亂數個數 */

// bg_rand 為 yes 時，則各項背景將成為亂數

// 顏色與背景設定
$indexbg = "0tree_bg.jpg";	/* 主頁背景 */
$enterbg = "2tree_bg.jpg";	/* 登入背景 */
$usebg = "3tree_bg.jpg";	/* 使用說明背景 */
$adminbg = "5tree_bg.jpg";	/* 帳號管理背景 */

$titletable = "#ffffff";	/* 主頁公告表單顏色 */ 
$titlerow = "#88c3ec";		/* 主頁公告表單第一列顏色 */ 
$titlerow1 = "#f6f6f6";		/* 主頁公告表單單數列顏色 */ 
$titlerow2 = "#bbceeb";		/* 主頁公告表單偶數列顏色 */ 
$titlepart = "#883322";		/* 主頁公告的單位顏色 */
$titledate = "blue";		/* 主頁公告的日期顏色 */
$titlesubject = "blue";		/* 主頁公告的標題顏色 */
$titlehit = "red";		/* 主頁公告的人氣顏色 */

$titleline1 = "#feb09d";	/* 主頁光棒的顏色 */
$titleline2 = "#f7cac9";	/* 主頁光棒的顏色 */

$addbg = "4tree_bg.jpg";		/* 發佈公告的背景顏色 */
$addtable = "#fef0ed";		/* 發佈公告的表單顏色 */

$showbg = "1tree_bg.jpg";	/* 看公告的背景 */
$showborder = "#014898";	/* 看公告的項目外框色 */
$showtbg1 = "#014898";		/* 看公告的項目背景色 */
$showt1 = "#ffffff";		/* 看公告的項目字體色 */
$showt2 = "#000000";		/* 看公告的項目內容字體色 */
$showtbg2 = "#fdfcdf";		/* 看公告的項目內容背景色 */
$showcbg = "#fef0ed";		/* 看公告的內容背景色 */
$showc = "#000000";		/* 看公告的內容字體色 */
$showpbg = "#f7cac9";		/* 看公告的備註背景色 */


// 公告等級顏色
$type1 = "black";		/* 普通 */
$type2 = "green";		/* 重要 */
$type3 = "red";			/* 急件 */
$type9 = "purple";			/* 更正 */

// 換行設定(一般來說，不用動)
$nl2br = "yes";			/* 設 yes 表用 php 的換行函式 */
				/* 或設其它換行字元，如 \n or \r */
// 認證 port
$pimapp = "imap:143";   /* imap imap_open() 的認證 port */
$ppopp = "pop3:110";    /* pop3 imap_open() 的認證 port */
$imapp = "143";         /* imap fsockopen() 的認證 port */
$popp = "110";          /* pop3 fsockopen() 的認證 port */


// 這是給 text 型態用的，不設採預設值 mysql 64k
$max_text_size = "";		/* 單位 kb */

$lang_ver = "utf-8";

$lang_title = "<meta http-equiv='Content-Type' content='text/html; Charset=".$lang_ver."'>";

// 使用自已的設定
include (dirname(__FILE__).'/config.inc.php');

if ($use_fckeditor == "yes" || $use_ckeditor == "yes")
  $add_check = "no";

/* 資料庫連結用，請勿修改 */
$my = mysql_pconnect($hostname, $username, $password);

if ($lang_ver == "utf-8")
  mysql_query("SET NAMES 'utf8'");

mysql_select_db($dbname, $my) or die("資料庫無法連結！"); 

/*Timezone*/
date_default_timezone_set('Asia/Taipei');
?>
