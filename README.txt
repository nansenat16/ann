

                           ANN 公告系統 (修正版) 
 			Base on 1.47 UTF-8 for PHP 5.3.3 or higher

◎ 原始作者
  程式設計：戴興能
  Origin：
          http://nextime.byethost4.com/

◎ 修正版
  程式設計：Nansen Su
  Project：https://bitbucket.org/nansenat16/ann

◎ 版權說明
  經原始作者同意，修正版與後續的修改採 GNU General Public License Version 3 授權方式釋出
  
  GPL License http://www.gnu.org/licenses/gpl.html
