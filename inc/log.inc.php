<?php
if (!empty($logfile))
{
  $nowtime = date("Y-m-d H:i:s");
  $logdata = "";

  if (!is_file($logfile))
    $logdata = "<?php exit ?>\n";

  $pwderr=isset($pwderr) ? $pwderr : 0;
  if ($pwderr == 1)
  {
    include ("inc/realip.inc.php");
    $logdata .= $nowtime." ".$myip." PWD_ERR ".$logmsg."\n";
  }
  else
  {
    include ("inc/auth.inc.php");

    if (isset($userid))
    {
      $link = "select partname from parttb where partid='".intval($partid)."'";
      $sql_act = "array";
      include ("inc/sql.inc.php");
      $logpartname = stripslashes($row['partname']);

      $link = "select username, realname from usertb where userid='".intval($userid)."'";
      $sql_act = "array";
      include ("inc/sql.inc.php");
      $logusername = $row['username'];
      $logrealname = stripslashes($row['realname']);

    }
    else
      $logpartname = $logusername = $logrealname = "超管";

    $logdata .= $nowtime." ".$logpartname." ".$logusername." ".$logrealname." ".$myip." ".$logmsg."\n";
  }

  if(is_writable($logfile)){
    $fp = fopen($logfile, "a");
    if($fp!==FALSE){
      fputs($fp, $logdata, strlen($logdata));
      fclose($fp);
    }
  }
}
?>
