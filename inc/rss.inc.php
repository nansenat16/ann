<?php
if ($rss_days)
{
  $rss_path = $uploadpath."/rss";
  $rss_file = $rss_path."/all.xml";
  $rss_time = date("D d M Y H:i:s");

  if (!is_dir($rss_path))
    mkdir($rss_path, 0755);

  $myrsstime = date("Y-m-d H:i:s", (time() - $rss_days * 86400));
  $link = "select * from titletb where (posttime > '$myrsstime') order by posttime desc";
  $sql_act = "number";
  include ("inc/sql.inc.php");

  $rss_data = "<?xml version=\"1.0\" encoding=\"". $lang_ver."\"?>

<rss version=\"2.0\">

<channel>
<title>". $myname ."</title>
<link>". $myhost ."</link>
<description>". $myname ."</description>
<language>zh-tw</language>
<lastBuildDate>". $rss_time ." GMT</lastBuildDate>
";

for ($i = 0; $i < $number; $i++)
{
  $sql_act = "row";
  include ("inc/sql.inc.php");
  $tid = $row['tid'];
  $partname = $row['partname'];
  $subject = stripslashes($row['subject']);
  $posttime = $row['posttime'];
  $firsttime = $row['firsttime'];
  $type = $row['type'];
  $mylocal = $row['local'];
  $mydate = explode(" ",$posttime);
  $mydate1 = explode("-",$mydate[0]);
  $mydate2 = explode(":",$mydate[1]);

  $mydatef = explode(" ",$firsttime);
  $mydatef = explode("-",$mydatef[0]);

  if ($mydatef[0] != "0000" && $use_update_type == "yes")
  {
    if (($type * 10) % 10 == 1)
      $type = 9.1;
    else
      $type = 9;
  }

  $mtype = intval($type);

  $rss_type[1] = "普通";
  $rss_type[2] = "重要";
  $rss_type[3] = "急件";
  $rss_type[9] = "更正";

  if ($rss_type_use == "yes")
    $localstr = "[".$rss_type[$mtype]."]";
  else
    $localstr = NULL;

  $issee = "ok";
  $my_local_type = NULL;

  if ($mylocal == "yes")
  {
    $issee = "no";
    $my_local_type = $rss_local_type;
    $islocal = strtok($local, " ");

    if ($out_see == "yes")
        $issee = "ok";

    while ($islocal && $issee == "no")
    {
      $tmp_end = substr($islocal, -1);
      $tmp_ip = explode(".", $islocal);

      if (strstr($tmp_ip[3], "-"))
      {
        $tmp_ipc = explode("-", $tmp_ip[3]);
        $tmp_ipk = $tmp_ip[0].".".$tmp_ip[1].".".$tmp_ip[2].".";
  
        if (preg_match('/^' . $tmp_ipk . '/i', $myip))
        {
          $tmp_ipf = explode(".", $myip);

          if ($tmp_ipc[0] <= $tmp_ipf[3] && $tmp_ipc[1] >= $tmp_ipf[3])
            $issee = "ok";
        }
      }
      else if (strstr($tmp_end, "."))
      {
        if (preg_match('/^' . $islocal . '/i', $myip))
          $issee = "ok";
      }
      else
      {
        if (strstr($islocal, $myip))
          $issee = "ok";
      }

      $islocal = strtok(" ");
    }
  }

  $mynew = mktime($mydate2[0], $mydate2[1], $mydate2[2], $mydate1[1], $mydate1[2], $mydate1[0]) - 8 * 3600;
  $rss_time = date("D d M Y H:i:s", $mynew);

  $tmpa = $myhost."/show.php?mytid=".$tid;

  if (($type * 10) % 10 == 1 && $issee == "ok")
  {
    $rss_data .= "
<item>
<title>".$localstr.$subject.$my_local_type."</title>
<link>".$tmpa."</link>
<author>". $partname ."</author> 
<pubDate>".$rss_time." GMT</pubDate>
</item>
";
  }


}

  $rss_data .= "
</channel>
</rss>
   ";

  $fp = fopen($rss_file, "w+");
  fputs($fp, $rss_data, strlen($rss_data));
  fclose($fp);
}
?>
