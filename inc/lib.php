<?php

if(!function_exists('val')){
	function val(&$var,&$val){
		if(isset($val)){$var=$val;}
	}
}

if (!function_exists('ann_error')){
	function ann_error($str, $exitok, $html_header=false){
		global $myname;
$html=<<<EOF
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; Charset=utf-8' />
	<link rel="stylesheet" type="text/css" href="./style.css">
	<title>%s</title>
</head>
<body>
EOF;
		if($html_header){echo sprintf($html,$myname);}
		echo '<div class="err_msg">' . htmlspecialchars($str) . '</div>';
		if ($exitok){
			echo '<div class="err_msg"><a href="index.php">返回' . $myname . '首頁</a></div></body></html>';
			exit();
		}
	}
}

if(!function_exists('str_filter')){
	function str_filter($str){
		if(PHP_VERSION>=6 || !get_magic_quotes_gpc()){
			return addslashes($str);
		}
		return $str;
	}
}

?>