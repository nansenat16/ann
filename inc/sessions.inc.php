<?php
$HANDER = NULL;
$LIFETIME = get_cfg_var("session.gc_maxlifetime");

if (empty($LIFETIME))
  $LIFETIME = 1440;

function sessionOpen($save_path, $session_name)
{
  return true;
}

function sessionClose()
{
  return true;
}

function sessionRead($session_key)
{
  global $session, $my;

  $session_key = addslashes($session_key);

  $sql = "select session_value from sessions where session_key='$session_key'";
  $rs = mysql_query($sql, $my) or die(mysql_error());

  if (mysql_numrows($rs) == 1)
  {
    $row = mysql_fetch_array($rs);
    return $row['session_value'];
  }

  return false;
}

function sessionWrite($session_key, $val)
{
  global $session, $nowtimestamp, $my;
  
  $session_key = addslashes($session_key);
  $val = addslashes($val);

  $sql = "select * from sessions where session_key='$session_key'";
  $rs = mysql_query($sql, $my) or die(mysql_error());

  if (mysql_numrows($rs) == 0)
  {
    $sql = "insert into sessions (session_key, session_expire, session_value) values ('$session_key', '$nowtimestamp', '$val')";
    $rs = mysql_query($sql, $my) or die(mysql_error());
  }
  else
  {
    $sql = "update sessions set session_expire='$nowtimestamp', session_value='$val' where session_key='$session_key'";
    $rs = mysql_query($sql, $my) or die(mysql_error());
  }

  return true;
}

function sessionDestroyer($session_key)
{
  global $session, $my;

  $session_key = addslashes($session_key);

  $sql = "delete from sessions where session_key='$session_key'";
  $rs = mysql_query($sql, $my) or die(mysql_error());

  return true;
}

function sessionGc($maxlifetime)
{
  global $session, $nowtimestamp, $my;

  $expirationTime = $nowtimestamp - $maxlifetime;

  $sql = "delete from sessions where session_expire < '$expirationTime'";
  $rs = mysql_query($sql, $my) or die(mysql_error());

  return true;
}

session_set_save_handler(
	'sessionOpen',
	'sessionClose',
	'sessionRead',
	'sessionWrite',
	'sessionDestroyer',
	'sessionGc'
);

?>
