<?php
$r_ip = $_SERVER['REMOTE_ADDR'];                // 一般 IP
$h_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';       // 有用 Porxy 的詳細 IP

$myip = $h_ipt = $r_ip;		// IP 預設用 REMOTE_ADDR
$islocalip = 0;			// 預設不是本地 NAT IP
$nip = explode(" ", $natip);	// 將 NAT IP 解開

for ($iip = 0; $iip<count($nip); $iip++)
  if ($nip[$iip] == $myip)
    $islocalip = 1;		// 是本地 NAT IP 

// 如果有提供 HTTP_X_FORWARDED_FOR 時，抓真實 ip 用，沒有則用 REMOTE_ADDR
if (strstr($h_ip, "."))
{
  $myip1 = explode(",", $h_ip);
  $myip = $myip1[0];	// 只要第一個的 ip 就好
  $h_ip_num = count($myip1);

// 取出最開始的真實 IP 為 h_ipt
  for ($jip = 0, $h_ip_stat = 1; $jip < $h_ip_num && $h_ip_stat == 1; $jip++)
  {
    $h_ip_stat = 0;
    $h_ipt2 = trim($myip1[$jip]);

    if (preg_match('/^(10|127|192\.168)\./', $h_ipt2))
      $h_ip_stat = 1;

    if(preg_match('/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\./', $h_ipt2))
        $h_ip_stat = 1;

    if ($h_ip_stat == 0)
      $h_ipt = $h_ipt2;
  }
}

// 是不是虛擬 ip
$isvip = 0;

if(preg_match('/^(10|192\.168|172\.((1[6-9])|(2[0-9])|(3[0-1])))\./',$myip))
  $isvip=1;

// 如果是虛擬 IP，又不是本地 NAT IP 時
if ($isvip == 1 && $islocalip == 0)
  $myip = $h_ipt;

?>
