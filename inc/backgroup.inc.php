<?php
function bg($op)
{
  if (preg_match('/\./', $op))
    $tmpbg = 'background="images/'.preg_replace('/[\*[:space:]]/', '', $op).'"';
  else
    $tmpbg = 'bgcolor="'.$op.'"';

  return $tmpbg;
}
?>
