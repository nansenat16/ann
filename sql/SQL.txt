

		 database 資料表與資料說明(以 mysql 為例)

// 群組用
  create table parttb (
    partid int(10) UNSIGNED DEFAULT '0' NOT NULL auto_increment, // 群組 index
    pid varchar(8) NOT NULL,            // 群組簡碼
    partname varchar(24) NOT NULL,      // 群組中文名
    partident varchar(64) NOT NULL,     // 群組備註
    rootuid int(10) DEFAULT '0',        // 這群組的群組長是誰
    PRIMARY KEY(partid)
    );


// 組員用
  create table usertb (
    userid int(10) UNSIGNED DEFAULT '0 ' NOT NULL auto_increment, // index
    uid char(32) DEFAULT 'test',        // safe_mode login uid 用
    partid int(10) UNSIGNED NOT NULL,   // 哪一群組
    username varchar(12) BINARY NOT NULL,       // 英文帳號
    realname varchar(12) NOT NULL,      // 中文姓名
    userpass varchar(12) NOT NULL,      // 密碼
    email varchar(64) NOT NULL,         // email
    userident varchar(64) NOT NULL,     // 組員備註
    PRIMARY KEY(userid)
    );


// 公告標題，主頁用
  create table titletb (
    tid int(10) UNSIGNED DEFAULT '0' NOT NULL auto_increment,
    partid int(10) UNSIGNED NOT NULL,   // 哪一群組的公告
    partname varchar(24) NOT NULL,      // 群組中文名
    subject varchar(64) NOT NULL,       // 標題
    posttime datetime NOT NULL,         // 公告的時間
    overtime datetime NOT NULL,         // 何時過期
    firsttime datetime DEFAULT '0' NOT NULL,  // 編輯公告後紀錄原先公告時間
    hits int(6) UNSIGNED DEFAULT '0' NOT NULL,  // 人氣指數
    type varchar(8) NOT NULL,           // 等級
    local varchar(8) DEFAULT 'no',	// 網內公告
    up int(1) UNSIGNED DEFAULT '0' NOT NULL, // 置頂
    PRIMARY KEY(tid)
    );


// 公告內容
  create table anntb (
    tid int(10) UNSIGNED NOT NULL,
    userid int(10) UNSIGNED NOT NULL,   // 誰發的公告
    ip varchar(20) NOT NULL,            // 連線地點
    filename varchar(255) NOT NULL,      // 附件
    url varchar(255) NOT NULL,           // 相關連結
    comment blob NOT NULL               // 內容
    );

