  create table parttb (
    partid int(10) UNSIGNED NOT NULL auto_increment,
    pid varchar(8) NOT NULL,
    partname varchar(24) NOT NULL,
    partident varchar(64) NOT NULL,
    rootuid int(10) DEFAULT '0',
    PRIMARY KEY(partid)
    ) DEFAULT CHARSET=utf8;

  create table usertb (
    userid int(10) UNSIGNED NOT NULL auto_increment,
    uid char(32) DEFAULT 'test',
    partid int(10) UNSIGNED NOT NULL,
    username varchar(12) BINARY NOT NULL,
    realname varchar(12) NOT NULL,
    userpass varchar(40) NOT NULL,
    email varchar(64) NOT NULL,
    userident varchar(64) NOT NULL,
    PRIMARY KEY(userid)
    ) DEFAULT CHARSET=utf8;

  create table titletb (
    tid int(10) UNSIGNED NOT NULL auto_increment,
    partid int(10) UNSIGNED NOT NULL,
    partname varchar(24) NOT NULL,
    subject varchar(255) NOT NULL,
    posttime datetime NOT NULL,
    overtime datetime NOT NULL,
    firsttime datetime NOT NULL,
    hits int(6) UNSIGNED DEFAULT '0' NOT NULL,
    type varchar(8) NOT NULL,
    local varchar(8) DEFAULT 'no',
    up int(1) UNSIGNED DEFAULT '0' NOT NULL,
    PRIMARY KEY(tid)
    ) DEFAULT CHARSET=utf8;

  create table anntb (
    tid int(10) UNSIGNED NOT NULL,
    userid int(10) UNSIGNED NOT NULL,
    ip varchar(20) NOT NULL,
    filename varchar(255) NOT NULL,
    url varchar(255) NOT NULL,
    comment blob NOT NULL
    ) DEFAULT CHARSET=utf8;

  create table sessions (
    session_key char(32) NOT NULL,
    session_expire int(11) UNSIGNED NOT NULL,
    session_value text NOT NULL,
    PRIMARY KEY (session_key)
    ) DEFAULT CHARSET=utf8;

