

                           ANN 公告系統
 			For Apache + PHP + MySQL Server

◎ 作者資訊
  程式設計：戴興能
  Origin：
          http://ann.isgreat.org
					http://anngo.co.cc
					http://one.now.to
					http://blog.xuite.net/nextime
          http://easytry.tyc.edu.tw/homepage.php?id=1432

◎ 版權說明
  完全免費，歡迎使用:)
  如散佈本程式，請保留所有的作者資訊和此版權說明，謝謝。

◎ 特殊功能
  1. 可單獨用 mypartid 指定特定群組，如

     http://anngo.co.cc/ann/index.php?mypartid=live

     上述之 live 為簡碼代號。

  2. 可配合其它 php 程式使用，

     <?
       $annpath = "../ann/";	/* 這程式根目錄到 ann 的相對路徑(最後請加 /) */
       include ($annpath."ezindex.php");
     ?>


◎ 使用 Xoops 密碼認證(2.013平台測的)
請在 conf/config.inc.php 最後加入

$usereal = "yes";                /* yes/no 使用 Unix/Xoops 帳號密碼 */
$xoopspath = "../xoops";        // Xoops 的路徑(mainfile.php 放在哪)
$userauth = "xoops";

在 ANN 首頁不要選群組，直接按[帳號管理]用超管帳號密碼進去，
建立群組和 xoops 對應帳號即可，這樣就會用 xoops 認證啦~~~ ^o^

◎ 要使用內部公告，一定要裝 ezf123 或 xoops，並在 config.inc.php 中設

$out_see = "yes";    /* no/yes 讓成員在外面可看內部公告 */

// local 變數是內部公告給特定的網域或 IP 可直接不用認證看用，請用空白隔開，如
// $local = "192.168.0.1 192.168.1. 192.168.2.1-125";
$local = "your.lan.ip"; // 不用認證的 ip
