<?php
  include ("conf/defconf.inc.php");
  include ("inc/backgroup.inc.php");
  include ("inc/bgrand.inc.php");

  echo $lang_title;
?>

<html>
<head>
<title>ANN 公告系統說明</title>
</head>
<?php
  if ($bg_rand != "yes")
    $mybg = bg($usebg);
  else
    $mybg = bg($outbg);
  echo "<body ".$mybg.">";
?>
　　　　　　<font color="red" size="5">ANN 公告系統</font> (For Apache + PHP + MySQL Server)<br><hr>
<br>
● 使用搜尋<br>
　一、如果沒有選擇哪一群組時，表示是找全部的群組。<br>
　二、請將標題或作者姓名的關鍵字輸在"搜尋"的框框內，再選擇有效天數，按確定即可。<br>
　三、也可以在年月份中輸入西元年(或民國年)和月份調閱該年度或月份公告。<br>
　四、當天數和年月份一起輸入時，以年月份優先處理。<br>
<br>
● 登入<br>
　一、可按《請選擇》選擇您屬哪一群組，請輸入您的帳號和密碼進行登入。<br>
　二、如有群組簡碼時，可直接點選"發佈公告"或"帳號管理"進行快速登入。<br>
　三、帳號和密碼大小寫不同且均為十個字元為限。<br>
<br>
● 發表公告<br>
　一、標題和內容一定要填寫。<br>
　二、如有附件時，請注意檔案大小和檔案名稱不可有空白或 < 或 ' 等特殊符號。<br>
　三、依照您公告內容需求調整公告有效時間。<br>
　四、換行請按 Enter，大約每行以四十字為優。<br>
　五、過期的公告並不會刪除，本意是做日後查詢用，如要刪除，可在看公告時，點選"刪除"，<br>
　　　或在管理模式中，有清理所有過期公告的選項進行大規模刪除。<br>
<br>
● 管理模式<br>
　一、請注意 <font color="red">★</font> 號的說明(有出現<font color="red">★</font>號時，表該欄是一定要填寫的)。<br>
　二、離開時，請確實點選[登出並返回公告系統]。<br>
<br><hr>

◎ 程式下載： <a href="https://bitbucket.org/nansenat16/ann">https://bitbucket.org/nansenat16/ann</a><br>

</body>
</html>
